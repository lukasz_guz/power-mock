package pl;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.concurrent.Executor;

import org.easymock.EasyMock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.api.easymock.internal.invocationcontrol.EasyMockMethodInvocationControl;
import org.powermock.api.support.membermodification.strategy.MethodStubStrategy;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({MyClass1.class, MyClass2.class})

public class MyClass1Test {

	@Test
	public void test() {
		PowerMock.stub(PowerMock.method(MyClass1.class, "hello")).toReturn("wapno");
		System.out.println("MyClass1. hello: " + MyClass1.hello("ziomn"));
		System.out.println("-------------------------");

		
		MyClass1 myClass1 = new MyClass1();
		PowerMock.stub(PowerMock.method(MyClass1.class, "c")).toReturn("Mock metody c");
		System.out.println(myClass1.c("tekst"));
		System.out.println("-------------------------");

		
		
		MyClass2 myClass2 = PowerMock.createMock(MyClass2.class);
		EasyMock.expect(myClass2.a("elo")).andReturn("Zwracam mock!").anyTimes();
		EasyMock.expect(myClass2.a(EasyMock.not(EasyMock.eq("elo")))).andReturn("innyMock!");
		EasyMock.expect(myClass2.c(EasyMock.anyString())).andReturn("Zmocowany mock!");
		EasyMock.replay(myClass2);
		
		
		System.out.println("MyClass2: " + myClass2.a("elo"));
		System.out.println("MyClass2: " + myClass2.a("inny"));
		System.out.println("MyClass2: " + myClass2.c("inny"));
		EasyMock.verify(myClass2);
		System.out.println("-------------------------");
		
	}
	
	public boolean a() {
		try {
			return true;
		} finally {
			return false;
		}
	}
	
	@Test
	public void test1() {
		BigDecimal a = new BigDecimal("2.03");
		BigInteger b = new BigInteger("3");
		BigDecimal c = new BigDecimal(b);
		a = a.add(c);
		System.out.println(a); // 5.03
		System.out.println(b); // 3
		System.out.println(c); // 3
		
		System.out.println(a());

	}
}