package pl;

public class MyClass1 {

	static String hello(String arg) {
		return "Hello " + arg;
	}

	private String goodbye(String arg) {
		return "Goodbye " + arg;
	}
	
	public String a() {
		return "MyClass1.a()";
	}
	
	public int b() {
		return 43;
	}
	
	public String c(String napis) {
		return "MyClass1.c(): " + napis;
	}
}